package ist.challenge.sirajuddin.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ist.challenge.sirajuddin.request.CreateUserRequest;
import ist.challenge.sirajuddin.request.LoginUserRequest;
import ist.challenge.sirajuddin.request.UpdateUserRequest;
import ist.challenge.sirajuddin.response.Result;
import ist.challenge.sirajuddin.response.ResultData;
import ist.challenge.sirajuddin.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("api/users")
public class UserController {
    
    @Autowired
    UserService service;

    @PostMapping("/register")
    public ResponseEntity<Result> createUser(@Valid @RequestBody CreateUserRequest regisUser) {
        return service.register(regisUser);
    }

    @PostMapping("/login")
    public ResponseEntity<Result> loginUser(@RequestBody LoginUserRequest loginUser) {
        return service.login(loginUser);
    }

    @GetMapping
    public ResponseEntity<ResultData> getUser() {
        return service.getUser();
    }

    @PutMapping
    public ResponseEntity<Result> updateUser(@Valid @RequestBody UpdateUserRequest updateUser) {
        return service.updateUser(updateUser);
    }
}
