package ist.challenge.sirajuddin.response;

import java.util.Arrays;

public class Result {
    
    protected int code;
    protected boolean success;
    protected String message;

    public Result() {
        this.code = 200;
        this.success = true;
        this.message = "Success";
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
