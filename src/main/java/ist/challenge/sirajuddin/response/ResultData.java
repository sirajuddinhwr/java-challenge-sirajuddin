package ist.challenge.sirajuddin.response;

import java.util.Arrays;

public class ResultData extends Result {

    private Object data;

    public ResultData() {
        super();
        this.data = Arrays.asList();
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    
}
