package ist.challenge.sirajuddin.service;

import org.springframework.http.ResponseEntity;

import ist.challenge.sirajuddin.request.CreateUserRequest;
import ist.challenge.sirajuddin.request.LoginUserRequest;
import ist.challenge.sirajuddin.request.UpdateUserRequest;
import ist.challenge.sirajuddin.response.Result;
import ist.challenge.sirajuddin.response.ResultData;

public interface UserService {

    ResponseEntity<Result> register(CreateUserRequest regisData);

    ResponseEntity<Result> login(LoginUserRequest loginData);

    ResponseEntity<ResultData> getUser();

    ResponseEntity<Result> updateUser(UpdateUserRequest updateData);

}
