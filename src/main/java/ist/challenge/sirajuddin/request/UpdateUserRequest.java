package ist.challenge.sirajuddin.request;

import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

public class UpdateUserRequest {

    @ApiModelProperty(position = 1, example = "1", required = true)
    private Long id;

    @ApiModelProperty(position = 2, example = "username", required = true)
    @Size(max = 25)
    private String username;

    @ApiModelProperty(position = 3, example = "password", required = true)
    @Size(max = 25)
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
