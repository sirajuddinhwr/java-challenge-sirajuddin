package ist.challenge.sirajuddin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ist.challenge.sirajuddin.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
    @Query("select count(*) as jumlah from User as s where lower(s.username) = ?1")
    int findUsername(String username);

    @Query("select count(*) as jumlah from User as s where s.id != ?1 and lower(s.username) = ?2")
    int findUsernameForUpdate(long id, String username);

    @Query("select count(*) as jumlah from User as s where lower(s.password) = ?1")
    int findPassword(String password);
}
