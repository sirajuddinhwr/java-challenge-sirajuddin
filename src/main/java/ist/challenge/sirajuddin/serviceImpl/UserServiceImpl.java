package ist.challenge.sirajuddin.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ist.challenge.sirajuddin.model.User;
import ist.challenge.sirajuddin.repository.UserRepository;
import ist.challenge.sirajuddin.request.CreateUserRequest;
import ist.challenge.sirajuddin.request.LoginUserRequest;
import ist.challenge.sirajuddin.request.UpdateUserRequest;
import ist.challenge.sirajuddin.response.Result;
import ist.challenge.sirajuddin.response.ResultData;
import ist.challenge.sirajuddin.service.UserService;

@Service
public class UserServiceImpl implements UserService {
    
    @Autowired
    UserRepository repository;

    private Result result;
    private ResultData resultData;

    @Override
    public ResponseEntity<Result> register(CreateUserRequest regisData) {
        result = new Result();

        try {
            if (repository.findUsername(regisData.getUsername()) > 0) {
                result.setCode(HttpStatus.CONFLICT.value());
                result.setSuccess(false);
                result.setMessage("Username sudah terpakai");
                return ResponseEntity.status(HttpStatus.CONFLICT.value()).body(result);
            }

            User newUser = new User(regisData.getUsername(), regisData.getPassword());
            repository.save(newUser);
            result.setCode(HttpStatus.CREATED.value());
            result.setMessage("Registrasi berhasil");
        } catch (Exception e) {
            result.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setSuccess(false);
            result.setMessage("Internal Server Error");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(result);
        }

        return ResponseEntity.status(HttpStatus.CREATED.value()).body(result);
    }

    @Override
    public ResponseEntity<Result> login(LoginUserRequest loginData) {
        result = new Result();

        try {
            boolean isEmpty = loginData.getUsername().isEmpty() || loginData.getPassword().isEmpty();
            if (isEmpty) {
                result.setCode(HttpStatus.BAD_REQUEST.value());
                result.setSuccess(false);
                result.setMessage("Username dan / atau password kosong");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(result);
            }

            boolean notMatch = (
                (repository.findUsername(loginData.getUsername()) <= 0)
                ||
                (repository.findPassword(loginData.getPassword()) <= 0)
            );
            if (notMatch) {
                result.setCode(HttpStatus.UNAUTHORIZED.value());
                result.setSuccess(false);
                result.setMessage("Username atau password tidak cocok");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED.value()).body(result);
            }

            result.setCode(HttpStatus.OK.value());
            result.setSuccess(true);
            result.setMessage("Sukses login");
        } catch (Exception e) {
            result.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setSuccess(false);
            result.setMessage("Internal Server Error");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(result);
        }
        
        return ResponseEntity.status(HttpStatus.OK.value()).body(result);

    }

    @Override
    public ResponseEntity<ResultData> getUser() {
        resultData = new ResultData();

        try {
            List<User> data = repository.findAll(Sort.by(Sort.Direction.ASC, "id"));

            if (data.isEmpty()) {
                resultData.setCode(HttpStatus.NOT_FOUND.value());
                resultData.setSuccess(false);
                resultData.setMessage("Belum ada data user");
                return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(resultData);
            }

            resultData.setCode(HttpStatus.OK.value());
            resultData.setSuccess(true);
            resultData.setMessage("Data didapatkan");
            resultData.setData(data);
        } catch (Exception e) {
            result.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setSuccess(false);
            result.setMessage("Internal Server Error");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(resultData);
        }

        return ResponseEntity.status(HttpStatus.OK.value()).body(resultData);
    }

    @Override
    public ResponseEntity<Result> updateUser(UpdateUserRequest updateData) {
        result = new Result();

        try {
            Optional<User> oldUser = repository.findById(updateData.getId());

            if (!oldUser.isPresent()) {
                result.setCode(HttpStatus.NOT_FOUND.value());
                result.setSuccess(false);
                result.setMessage("Data tidak ditemukan");
                return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(result);
            }

            if (repository.findUsernameForUpdate(updateData.getId(), updateData.getUsername()) > 0) {
                result.setCode(HttpStatus.CONFLICT.value());
                result.setSuccess(false);
                result.setMessage("Username sudah terpakai");
                return ResponseEntity.status(HttpStatus.CONFLICT.value()).body(result);
            }

            if (updateData.getPassword().equals(oldUser.get().getPassword())) {
                result.setCode(HttpStatus.BAD_REQUEST.value());
                result.setSuccess(false);
                result.setMessage("Password tidak boleh sama dengan password sebelumnya");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(result);
            }

            User user = new User(
                updateData.getId(),
                updateData.getUsername(),
                updateData.getPassword()
            );
            repository.save(user);
            result.setCode(HttpStatus.CREATED.value());
            result.setSuccess(true);
            result.setMessage("Berhasil update data");
        } catch (Exception e) {
            result.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            result.setSuccess(false);
            result.setMessage("Internal Server Error");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(result);
        }

        return ResponseEntity.ok(result);
    }

}
